import { useState } from "react";
import React from "react";
import { View, Button } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const DateTimePicker = ({ type, buttonTitle, dateKey, setSchedule }) => {
  const [datePickerVisible, setdatePickerVisible] = useState(false);
  const showDatePicker = () => {
    setdatePickerVisible(true);
  };
  const hideDatePicker = () => {
    setdatePickerVisible(false);
  };
  const handleConfirm = (date) => {
    if (type === "time") {
      //lógica para extrair hora e minuto
      const hour = date.getHours();
      const minute = date.getMinutes();

      //lógica para montar hora e minuto no formatado
      const formatedTime = `${hour}:${minute}`;
      setSchedule((prevState) => ({
        ...prevState,
        [dateKey]: formatedTime,
      }));
    } else {
        const formattedDate = date.toISOString().split('T')[0];
      setSchedule((prevState) => ({
        ...prevState,
        [dateKey]: formattedDate,
      }));

      //prevstate mantém o estado do resto e muda só o o time
      //atualizar schedule
    }

    hideDatePicker();

   
  };

  return (
    <View>
      <Button title={buttonTitle} onPress={showDatePicker} color="red" />
      {/* //o nome do botão vai vir com base no que tiver ocorrendo */}
      <DateTimePickerModal
        isVisible={datePickerVisible}
        mode={type}
        locale="pt_BR"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
    </View>
  ); // fim do return
}; // fim da DateTimePicker
//pegar data e hora, ou somente hora

export default DateTimePicker;
